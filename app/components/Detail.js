var React = require('react');
var PropTypes = require('prop-types');
var queryString = require('query-string');
var utils = require('../utils/helpers.js');
var getDate = utils.getDate;
var convertTemp = utils.convertTemp;

class Detail extends React.Component {
	
	componentWillMount () {
		this.index = queryString.parse(this.props.location.search)._k;
		this.info = this.props.location.state.info;
		
		// console.log('index');
		// console.log(queryString.parse(this.props.location.search)._k);
		// console.log(this.props.location.state.index);
	}
		
	render() {
		var info = this.info;
		var name = info.city.name;
		var day = info.list[this.index];
		var dateday = getDate(day.dt);
		var icon = day.weather[0].icon;
		var description = day.weather[0].description;
		var mintemp = convertTemp(day.temp.min);
		var maxtemp = convertTemp(day.temp.max);
		var humidity = day.humidity;
		return (
			<div className='description-container' style={{marginTop: '35px'}}>
				<img 
					className='weather'
					src={'../app/images/weather-icons/' + icon + ".svg"}
					alt={description}
				/>
				<h2 className='subheader'>{dateday}</h2>
				<p>{name}</p>
				<p>{description}</p>
				<p>min temp: {mintemp} degrees</p>
				<p>max temp: {maxtemp} degrees</p>
				<p>humidity: {humidity}</p>
			</div>
			
		)
	}
}

module.exports = Detail;