var React = require('react');
var Link = require('react-router-dom').Link;
var Zipsearch = require('./Zipsearch');

var api = require('../utils/api.js');
var axios = require('axios');
// import Banner from 'react-svg-loader!../images/pattern.svg';

class Home extends React.Component {
	

	render() {
		return (
			<div 
				className='home-container'
				style={{backgroundImage: "url('app/images/pattern.svg')"}}
				>
					<h1 className='header'>Enter a City and State</h1>
					<Zipsearch match={this.props.match}/>
					
			</div>
		)
	}
}

module.exports = Home;