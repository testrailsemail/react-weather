var React = require('react');
var PropTypes = require('prop-types');
var ReactRouter = require('react-router-dom');
var Router = ReactRouter.BrowserRouter;
var Route = ReactRouter.Route;
var Switch = ReactRouter.Switch;
var Nav = require('./Nav');
var Home = require('./Home');
var Forecast = require('./Forecast');
var Detail = require('./Detail');

class App extends React.Component {
	render() {
		return (
      <Router>
        <div className='container'>
          <Nav />
          <Switch>
            <Route exact path='/' component={Home} />
            <Route path='/forecast' component={Forecast} />
            <Route path='/details/:city' component={Detail} />
            <Route render={ function () {
                return <p>Not found</p>
            }} />
          </Switch>
        </div>
      </Router>
		)
	}
}

module.exports = App;
