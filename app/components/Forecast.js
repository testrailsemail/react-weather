var React = require('react');
var PropTypes = require('prop-types');
var queryString = require('query-string');
var api = require('../utils/api.js');
var Loading = require('./Loading');

var utils = require('../utils/helpers.js');
var getDate = utils.getDate;
var convertTemp = utils.convertTemp;

function CastGrid(props) {
	return (
		<div className="forecast-container">
			{props.info.list.map(function(day, index) {
				return (
					<div key={day.dt} className='forecast-item' onClick={props.handleSubmit.bind(null, props.info, index)}>
						<img 
							className='weather'
							src={'app/images/weather-icons/' + day.weather[0].icon + ".svg"}
							alt={day.weather[0].description}
						/>
						<h2 className="subheader">{getDate(day.dt)}</h2>
					</div>
				)
			})}
		</div>
	)
}

class Forecast extends React.Component {
	constructor(props) {
		super (props);
		this.state = {
			info: null,
			error: null,
			loading: true
		}
		this.updateProps = this.updateProps.bind(this);
	}
	updateProps(props) {
		var searchfor = queryString.parse(props.location.search);
		console.log(searchfor)
		api.forecast(searchfor.city).then(function (results) {
			if (results === null) {
				return this.setState(function () {
					return {
						error: "there was an error. Check that both users exist on Github.",
						loading: false
					} 
				})
			}

			this.setState(function () {
				return {
					error: null,
					loading: false,
					info: results
				}
			})
		}.bind(this))
		this.handleSubmit = this.handleSubmit.bind(this);
	}
	componentDidMount () {
		// console.log('componentDidMount');
		this.updateProps(this.props)
	}
	componentWillReceiveProps(nextProps) {
		// console.log('componentWillReceiveProps');
		this.updateProps(nextProps)
	}
	handleSubmit(info, index) {
		var city = info.city.name;
		this.props.history.push({
      pathname: '/details/' + city,
      search: '?_k=' + index,
      state: {info, index}
    })
		// window.location = '/details/' + city + '?_k=' + index;
	}
	render() {
		var match = this.props.match;
		var info = this.state.info;
		var error = this.state.error;
		var loading = this.state.loading;
		// var  = this.state.;

		if (loading === true) {
			return <Loading />
		}

		if (error) {
			return (
				<div>
					<p>{error}</p>
					<Link to='/'>Reset</Link>
				</div>
			)	
		}
		return (
			<div>
				<h1 className='forecast-header'>{info.city.name}</h1>
				<CastGrid info={info} handleSubmit={this.handleSubmit}/>
				{console.log("done")}
			</div>
			
		)
	}
}

module.exports = Forecast;