var React = require('react');
var PropTypes = require('prop-types');
var Link = require('react-router-dom').Link;

var api = require('../utils/api.js');
var axios = require('axios');


class Zipsearch extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			city: '',
			wdata: null,
			error: null,
			loading: true
		};
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}
	handleSubmit() {
		var value = this.state.city;
		api.weather(value)
			.then(function (forecast) {
				this.setState(function() {
					return {
						wdata: forecast
					}
				})
			}.bind(this));
		
	}
	handleChange(event) {
		var value = event.target.value;
		this.setState(function(){
			return {city: value}
		})
	}
	render() {
		var match = this.props.match;
		var city = this.state.city;
		var wdata = this.state.wdata;
		return (
			<div className='zipcode-container' style={{flexDirection: this.props.direction}}>
				<input 
					className='form-control'
					placeholder='St. George, Utah'
					type='text'
					autoComplete='off'
					value={this.state.city}
					onChange={this.handleChange}
				/>
				<Link
					className='btn btn-success'
					type='button'
					style={{margin: "10px"}}
					to={{ 
						pathname: this.props.match.url +'forecast',
						search: '?city=' + city
					}}>
						Get Weather
				</Link>
				   { //console.log(this.props)
				   }
			</div>
		)
	}
}

Zipsearch.defaultProps = {
  direction: 'column',
  match: {url: '/'}
}

Zipsearch.propTypes = {
  direction: PropTypes.string,
}
	
module.exports = Zipsearch;