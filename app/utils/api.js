var axios = require('axios');

var apikey = "593efa0bc80316932300a25d3b7b6499";// "b714ec74bbab5650795063cb0fdf5fbe";
var curweather = "http://api.openweathermap.org/data/2.5/weather?q=";
var forecast = "http://api.openweathermap.org/data/2.5/forecast/daily?q=";
var fcastparam = "&cnt=5";
var params = "&type=accurate&APPID=" + apikey;

function getForecast (city) {
	return axios.get(forecast + city + params + fcastparam)
		.then(function (cast) {
			return cast.data;
		});
}

function getWeather (city) {
	return axios.get(curweather + city + params)
		.then(function (forecast) {
			return forecast.data;
		});
}

// function getStarCount (repos) {
// 	return repos.data.reduce(function (count, repo) {
// 		return count + repo.stargazers_count
// 	}, 0);
// }

// function calculateScore (profile, repos) {
// 	var followers = profile.followers;
// 	var totalStars = getStarCount(repos);

// 	return (followers * 3) + totalStars;
// }

function handleError (error) {
	console.warn(error);
	return null;
}

// function getUserData (player) {
// 	return axios.all([
// 		getProfile(player),
// 		getRepos(player)
// 	]).then(function (data) {
// 		var profile = data[0];
// 		var repos = data[1];

// 		return {
// 			profile: profile,
// 			score: calculateScore(profile, repos)
// 		}
// 	})
// }

// function sortPlayers (players) {
// 	return players.sort(function (a,b) {
// 		return b.score - a.score;
// 	});
// }

module.exports = {
	forecast: function (city) {
		return getForecast(city)
			.catch(handleError)
	},
	// forecast: function (city) {
	// 	return axios.all(getForecast(city))
	// 		.catch(handleError)
	// },
	weather: function (city) {
		return getWeather(city)
			.catch(handleError)
	}
}